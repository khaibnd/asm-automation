'''
Calculate raw material requirement

Index(['Mã sản phẩm', 'Mã S.Phẩm KH', 'Mã SP China', 'Tên sản phẩm',
       'Mã vật tư', 'Tên vật tư', 'Đvt', 'Sử dụng?', 'Số lượng',
       'Nhóm vtư thay thế', 'Hao hụt khi mua (%)', 'Hao hụt khi SX (%)',
       'Là bán T.Phẩm?', 'Mã ASM TQ', 'Tên vtư khác', 'Ghi chú'],

Mã sản phẩm	Mã S.Phẩm KH	Mã SP China	Tên sản phẩm	Mã vật tư	Tên vật tư	Đvt	Sử dụng?	Số lượng	Nhóm vtư thay thế	Hao hụt khi mua (%)	Hao hụt khi SX (%)

'''


import os
import numpy as np
import pandas as pd

from datetime import datetime as dt


def mrp_caculation():
    print('MRP calculation')

    report_time = dt.now().strftime("%Y%m%d %H%M%S")
    input_src = "LinkQ_data/"
    output_src = "Output/"

    # BOM Modification

    BOM_col = ['Mã sản phẩm', 'Mã vật tư', 'Tên vật tư', 'Đvt', 'Sử dụng?', 'Số lượng',
       'Nhóm vtư thay thế', 'Hao hụt khi mua (%)', 'Hao hụt khi SX (%)',
       'Là bán T.Phẩm?']

    BOM_file = os.path.join(input_src, "BOM.xls")
    raw_BOM_df = pd.read_excel(BOM_file, sheet_name='Sheet1', skiprows=4)
    raw_BOM_df = raw_BOM_df[BOM_col]

    BOM_output = "BOM_" + report_time + ".xlsx"
    BOM_writer = pd.ExcelWriter(os.path.join(output_src, BOM_output))

    raw_BOM_df.to_excel(BOM_writer, sheet_name='Raw BOM', index=False)

    semi_df = raw_BOM_df.loc[raw_BOM_df['Là bán T.Phẩm?']==True]
    semi_df.to_excel(BOM_writer, sheet_name='Semi', index=False)

    alternative_df = raw_BOM_df.loc[~raw_BOM_df['Nhóm vtư thay thế'].isna()]
    alternative_df.to_excel(BOM_writer, sheet_name='Alternative', index=False)

    BOM_df = raw_BOM_df.loc[raw_BOM_df['Sử dụng?']==True]
    BOM_df = BOM_df[['Mã sản phẩm', 'Mã vật tư', 'Tên vật tư', 'Đvt', 'Số lượng']]
    BOM_df.to_excel(BOM_writer, sheet_name='BOM', index=False)

    BOM_writer.save()

    # Order modification

    order_col = ['Customer', 'PO', 'PO Receive', 'Commit Date', 'VN Part', 'Material Release', 'Plan']
    order_file = os.path.join(input_src, "MPS.xlsx")

    FG_lst = BOM_df['Mã sản phẩm'].unique().tolist()

    raw_order_df = pd.read_excel(order_file, sheet_name='Open Orders', skiprows=0)
    raw_order_df = raw_order_df[order_col]

    order_output = "Order_" + report_time + ".xlsx"
    order_writer = pd.ExcelWriter(os.path.join(output_src, order_output))

    raw_order_df.to_excel(order_writer, sheet_name='Raw order', index=False)
    raw_order_df = raw_order_df.loc[raw_order_df['Material Release'] > 0]
    raw_order_df['Plan'] = pd.to_datetime(raw_order_df['Plan'], utc=False, errors="coerce")

    check_order_df = raw_order_df[raw_order_df['Plan'].isna()]
    check_order_id_lst = check_order_df.index.values.tolist()
    check_order_df['Remark'] = 'Pending/Cancel'

    check_FG_df = raw_order_df[~raw_order_df['VN Part'].isin(FG_lst)]
    check_FG_df['Remark'] = 'Missing FG'

    print(check_FG_df)

    error_order_df = pd.concat([check_order_df, check_FG_df])

    error_order_df.to_excel(order_writer, sheet_name='Error', index=False)

    order_df = raw_order_df.loc[~raw_order_df.index.isin(check_order_id_lst)]
    order_df['Plan'] = order_df['Plan'].dt.strftime('%Y/%m/%d')
    order_df.to_excel(order_writer, sheet_name='order', index=False)
    
    order_writer.save()

    # MRP calculation

    MRP_output = "MRP_" + report_time + ".xlsx"
    MRP_writer = pd.ExcelWriter(os.path.join(output_src, MRP_output))

    merge_df = order_df.merge(BOM_df,left_on='VN Part', right_on='Mã sản phẩm', how='left')
    merge_df['Requirement'] = merge_df['Material Release'] * merge_df['Số lượng']
    merge_df.to_excel(MRP_writer, sheet_name='Raw', index=False)

    horizon_lst = merge_df['Plan'].unique().tolist()



    pivot_material_df = pd.pivot_table(merge_df, values='Requirement', index=['Mã vật tư', 'Tên vật tư', 'Đvt'], columns=['Plan'], aggfunc=np.sum).reset_index()
    pivot_material_df = pivot_material_df.reindex(['Mã vật tư', 'Tên vật tư', 'Đvt'] + horizon_lst, axis='columns')
    pivot_material_df.to_excel(MRP_writer, sheet_name='Material', index=False)

    MRP_writer.save()
    return None


if __name__ == '__main__':
    
    mrp_caculation()